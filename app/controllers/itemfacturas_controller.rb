class ItemfacturasController < ApplicationController
  def index
  end

  def show
  end

  def new
    @itemfactura = Itemfactura.new
  end

  def create
    @itemfactura = Itemfactura.new(params[:itemfactura])
    producto = Producto.find(@itemfactura.producto_id)
    if producto.stock - @itemfactura.cantidad >= 0
      @itemfactura.save
      producto.save
    else
      flash[:notice] = 'Errores en el stock. Revisar el stock'
      render :controller => "facturas", :action => "new"
    end 
  end

  def edit
  end

  def update
  end

  def destroy
  end
end
