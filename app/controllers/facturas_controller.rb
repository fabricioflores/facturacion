class FacturasController < ApplicationController

  def index
    @facturas = Factura.all
    @valor = Factura.where(:created_at => Time.now.beginning_of_day..Time.now.end_of_day).sum(:total)
  end

  def show
     @factura = Factura.find(params[:id])
  end

  def new
    @factura = Factura.new(:numero => Factura.last ? Factura.last.numero + 1 : 1 )
    @factura.itemfacturas.build
  end

  def create
    cliente_attrs = params[:factura].delete :cliente
    @cliente = cliente_attrs[:id].present? ? Cliente.update(cliente_attrs[:id],cliente_attrs) : Cliente.create(cliente_attrs)    
    if @cliente.save
      @factura = @cliente.facturas.build(params[:factura])
          if @factura.save
            redirect_to facturas_path, :notice => "Factura Guardada"
          else
            render "new"
          end
    else
        flash[:notice] = 'Errores en el cliente'
        @factura = Factura.new(:numero => Factura.last ? Factura.last.numero + 1 : 1 )
        @factura.itemfacturas.build
        render "new"
    end 
  end
end