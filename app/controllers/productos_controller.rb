class ProductosController < ApplicationController
  helper_method :sort_column, :sort_direction

  def index
    respond_to do |format|
        format.html {    
        presentacion
        }
       format.js{
        presentacion       
      }
        format.json { 
          @productos = Producto.where("descripcion like ?", "%#{params[:term]}%")
          @productos = @productos.map do |producto|
            {
              :id => producto.id,
              :label => producto.descripcion,
              :value => producto.descripcion,
              :precio => producto.precio
            }
          end
          render :json => @productos 
        }
      end
  end

  def show
    @producto = Producto.find(params[:id])
  end

  def new
    @producto = Producto.new
  end

  def create
    @producto = Producto.new(params[:producto])
    if @producto.save
      redirect_to productos_path, :notice => "Producto guardado"
    else
      render "new"
    end
  end

  def edit
    @producto = Producto.find(params[:id])
  end

  def update
    @producto = Producto.find(params[:id])
  
  if @producto.update_attributes(params[:producto]) 
    redirect_to productos_path, :notice => "Producto actualizado"
  else
    render "edit"
  end
  end

  def destroy
    @producto = Producto.find(params[:id])
    @producto.destroy
    redirect_to productos_path, :notice => "Producto eliminado"
  end

  private

  def sort_column
    Producto.column_names.include?(params[:sort]) ? params[:sort] : "id"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end

  def presentacion
    @productos = Producto.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page => params[:page], :per_page => 10)
  end

end
