class ClientesController < ApplicationController
  helper_method :sort_column, :sort_direction

  def index
     respond_to do |format|
        format.html {    
        presentacion
        }
       format.js{
        presentacion       
      }
        format.json { 
          @clientes = Cliente.where("cedula like ?", "%#{params[:term]}%")
          @clientes = @clientes.map do |cliente|
            {
              :id => cliente.id,
              :label => cliente.cedula,
              :value => cliente.cedula,
              :nombres => cliente.nombres,
              :direccion => cliente.direccion,
              :telefono => cliente.telefono
            }
          end
          render :json => @clientes 
        }
     end
  end


  def show
    @cliente = Cliente.find(params[:id])
  end

  def new
    @cliente = Cliente.new
  end

  def create
    @cliente = Cliente.new(params[:cliente])
    if @cliente.save
      redirect_to clientes_path, :notice => "Cliente guardado"
    else
      render "new"
    end
  end

  def edit
    @cliente = Cliente.find(params[:id])
  end

  def update
    @cliente= Cliente.find(params[:id])
  
  if @cliente.update_attributes(params[:cliente]) 
    redirect_to clientes_path, :notice => "Cliente actualizado"
  else
    render "edit"
  end
  end

  def destroy
    @cliente = Cliente.find(params[:id])
    @cliente.destroy
    redirect_to clientes_path, :notice => "Cliente eliminado"
  end
  
  private

  def sort_column
    Cliente.column_names.include?(params[:sort]) ? params[:sort] : "id"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end

  def presentacion
    @clientes = Cliente.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page => params[:page], :per_page => 10)
  end
end
