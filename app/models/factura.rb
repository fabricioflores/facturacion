class Factura < ActiveRecord::Base
  attr_accessible :cliente_attributes, :iva, :numero, :subtotal, :total, :created_at, :itemfacturas_attributes
  belongs_to :cliente
  has_many :itemfacturas
  has_many :productos, :through => :itemfacturas
  accepts_nested_attributes_for :itemfacturas, :reject_if => lambda { |a| a[:descripcion].blank? }, :allow_destroy => true

 def cliente_tokens=(id)
    self.cliente_id = id.split(",")
  end

 def cliente_cedula
  cliente.cedula if cliente
end

def cliente_nombre=(cedula)
  self.cliente = Cliente.find_or_create_by_cedula(cedula) unless cedula.blank?
end


end
