# encoding: utf-8
class Producto < ActiveRecord::Base
  attr_accessor :proveedor_producto

  attr_accessible :descripcion, :precio, :stock, :fecha_caducidad, :tipo, :proveedor_id, :proveedor_producto
  has_many :itemfacturas
  has_many :facturas, :through => :itemfacturas
  validates :descripcion, :precio, :stock, :fecha_caducidad, :tipo, :presence => true 
  validates :descripcion, :uniqueness => { :message => "Elija otra descripción" }
  belongs_to :proveedor

  def self.search(search)
  	if search
  		where('descripcion LIKE ?', "%#{search}%")
  	else
  		scoped
  	end
  	
  end

end
