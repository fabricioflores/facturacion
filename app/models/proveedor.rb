# encoding: utf-8
class Proveedor < ActiveRecord::Base
  attr_accessible :cedula, :direccion, :nombres, :telefono
  has_many :productos

  validates :cedula, :direccion, :nombres, :telefono, :presence => true 
  validates :cedula, :uniqueness => { :message => "Cedula ya en uso" }
  validates :cedula, :numericality => {:only_integer => true}
  validates :telefono, :numericality => {:only_integer => true}
  validates :cedula, :length => { :in => 10..13 }
  validate :verify_cedula

  def verify_cedula
    autoverifier = self.cedula[ self.cedula.size - 1 ].to_i
    index = sum = 0
    self.cedula.chars { |char|
       break if index == self.cedula.size - 1
       number = char.to_i
       coef = index.even? ? 2 : 1
       product = number * coef
       product -= 9 if product > 9
       sum += product
       index += 1
   }
   remainder = sum % 10
   booleano =  10 - remainder == autoverifier
  errors.add(:cedula, "Cédula no válida") if booleano == false
end
end
