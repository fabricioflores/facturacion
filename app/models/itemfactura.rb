class Itemfactura < ActiveRecord::Base
  attr_accessor :vu, :vt, :descripcion
  belongs_to :factura
  belongs_to :producto
  attr_accessible :cantidad, :producto_id, :vu, :vt, :descripcion, :producto, :factura, :precio_unitario, :valortotal
  validate :el_stock
  validate :caducado
  validates :cantidad, :numericality => {:greater_than => 0, :only_integer => true}


  def el_stock
    errors.add(:producto, "No hay stock suficiente para "+self.producto.descripcion+". Stock actual: "+self.producto.stock.to_s) if self.producto.stock - self.cantidad < 0
  end

  def caducado
  	errors.add(:producto, "Producto caducado "+self.producto.descripcion) if self.producto.fecha_caducidad < Time.now
  end

    end
