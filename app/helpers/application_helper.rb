module ApplicationHelper
	def sortable(column , title=false)
		title ||= column.titleize
		css_class = column == sort_column ? "current #{sort_direction}" : nil
		direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
		link_to title, params.merge(:sort => column, :direction => direction, :page => nil), {:class => css_class}	
	end

  # def add_child_link(name, form_builder, association)
  #   object = form_builder.object.class.reflect_on_association(association).klass.new
  #   partial = "#{association.to_s.singularize}_fields"
  #   template = content_tag(:div, :id => "#{association}_fields_template", :style => "display: none") do
  #     form_builder.fields_for(association, object, :child_index => "new_#{association}") do |f|
  #       render(:partial => partial, :locals => { :f => f })
  #     end
  #   end
  #   template + link_to(name, "javascript:void(0)", :class => "add_child", :"data-association" => association)
  # end

  # def remove_child_link(name, f)
  #   f.hidden_field(:_destroy) + link_to(name, "#", :class => "remove_child")
  # end
  def quid(price)
    number_to_currency(price, :unit => "$")
  end

end
