// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery-ui
//= require rails
//= require jquery_nested_form
//= require_tree ../../../vendor/assets/javascripts/
//= require_tree .


$(function() {
	$("#productos th a, #productos .pagination a").live("click", function() {
		$.getScript(this.href);
		return false;
	});
	$("#products_search input").keyup(function() {
		$.get($("#products_search").attr("action"), $("#products_search").serialize(), null, "script");
		return false;
	});
});

$(function() {
	$("#clientes th a, #clientes .pagination a").live("click", function() {
		$.getScript(this.href);
		return false;
	});
	$("#clientes_search input").keyup(function() {
		$.get($("#clientes_search").attr("action"), $("#clientes_search").serialize(), null, "script");
		return false;
	});
});

$(function() {
        $( "#factura_cliente_cedula" ).autocomplete({
            source: "/clientes.json",
            select: function( event, ui ) {
            		$("#factura_cliente_nombres").val(ui.item.nombres);
            		$("#factura_cliente_direccion").val(ui.item.direccion);
            		$("#factura_cliente_telefono").val(ui.item.telefono);
            		$("#factura_cliente_id").val(ui.item.id);
            }
    });
});

$(function() {
        $( "#producto_proveedor_producto" ).autocomplete({
            source: "/proveedors.json",
             select: function( event, ui ) {
                     $("#producto_proveedor_id").val(ui.item.id);
             }
    });
});


$(function() {
    $(document).on("focus",".desc_autocomplete", function() {
        $('.desc_autocomplete').autocomplete({
            source: "/productos.json",
            select: function( event, ui ) {
                    $this = $(this)
                    $this.siblings(".valorunitario").val(ui.item.precio);
                    $this.siblings(".valortotal").val($this.siblings(".cantidad").val()*ui.item.precio);
                    $this.siblings(".producto_id").val(ui.item.id);
                    var sum = 0;
                        $('.valortotal').each(function(){
                        sum += parseFloat(this.value);
                        });
                    $("#factura_subtotal").val(sum);
                    $("#factura_iva").val((sum*0.12).toFixed(2));
                    $("#factura_total").val((sum*0.12+sum).toFixed(2));
            }
        }); 
    });
});








