Facturacion::Application.routes.draw do
 

  resources :proveedors


  get "itemfacturas/index"

  get "itemfacturas/show"

  get "itemfacturas/new"

  get "itemfacturas/create"

  get "itemfacturas/edit"

  get "itemfacturas/update"

  get "itemfacturas/destroy"

  get "productos/index"

  get "productos/show"

  get "productos/new"

  get "productos/create"

  get "productos/edit"

  get "productos/update"

  get "productos/destroy"

  get "facturas/index"

  get "facturas/show"

  get "facturas/new"

  get "facturas/create"

  get "facturas/edit"

  get "facturas/update"

  get "facturas/destroy"

  get "clientes/index"

  get "clientes/show"

  get "clientes/new"

  get "clientes/create"

  get "clientes/edit"

  get "clientes/update"

  get "clientes/destroy"

  get "facturas/autocomplete_cliente_cedula"

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  resources :clientes
  resources :facturas
  resources :itemfacturas
  resources :productos

  resources :facturas do
    get :autocomplete_cliente_cedula, :on => :collection
  end

  

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'productos#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
