# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130223181338) do

  create_table "clientes", :force => true do |t|
    t.string   "cedula"
    t.string   "nombres"
    t.string   "direccion"
    t.string   "telefono"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "clientes", ["cedula"], :name => "index_clientes_on_cedula", :unique => true

  create_table "facturas", :force => true do |t|
    t.integer  "numero"
    t.decimal  "subtotal",   :precision => 10, :scale => 2
    t.decimal  "iva",        :precision => 10, :scale => 2
    t.integer  "cliente_id"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.decimal  "total",      :precision => 10, :scale => 2
  end

  create_table "itemfacturas", :force => true do |t|
    t.integer  "factura_id"
    t.integer  "producto_id"
    t.integer  "cantidad"
    t.decimal  "valortotal",      :precision => 10, :scale => 2
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
    t.decimal  "precio_unitario", :precision => 10, :scale => 2
  end

  add_index "itemfacturas", ["factura_id"], :name => "index_itemfacturas_on_factura_id"
  add_index "itemfacturas", ["producto_id"], :name => "index_itemfacturas_on_producto_id"

  create_table "productos", :force => true do |t|
    t.text     "descripcion"
    t.decimal  "precio",          :precision => 10, :scale => 2
    t.integer  "stock"
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
    t.datetime "fecha_caducidad"
    t.text     "tipo"
    t.integer  "proveedor_id"
  end

  create_table "proveedors", :force => true do |t|
    t.string   "nombres"
    t.string   "cedula"
    t.string   "direccion"
    t.string   "telefono"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
