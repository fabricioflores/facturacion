class ClientIndex < ActiveRecord::Migration
  def up
  	add_index :clientes, :cedula, :unique => true
  end

  def down
  	remove_index :clientes, :cedula
  end
end
