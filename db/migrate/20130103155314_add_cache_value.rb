class AddCacheValue < ActiveRecord::Migration
  def up
  	change_table :itemfacturas do |t|
  		t.float "precio_unitario"
  	end
  end

  def down
  	change_table :itemfacturas do |t|
  		t.remove "precio_unitario"
  	end
  end
end
