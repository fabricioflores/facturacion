class CreateItemfacturas < ActiveRecord::Migration
  def change
    create_table :itemfacturas do |t|
      t.references :factura
      t.references :producto
      t.integer :cantidad
      t.float :valortotal

      t.timestamps
    end
    add_index :itemfacturas, :factura_id
    add_index :itemfacturas, :producto_id
  end
end
