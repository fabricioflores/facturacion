class CreateClientes < ActiveRecord::Migration
  def change
    create_table :clientes do |t|
      t.string :cedula
      t.string :nombres
      t.string :direccion
      t.string :telefono
      t.timestamps
    end
  end
end
