class Facturapreciototal < ActiveRecord::Migration
  def up
  	change_table :facturas do |t|
  		t.float "total"
  	end
  end

  def down
  	change_table :facturas do |t|
  		t.remove "total"
  	end
  end
end
