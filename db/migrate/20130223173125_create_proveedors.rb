class CreateProveedors < ActiveRecord::Migration
  def change
    create_table :proveedors do |t|
      t.string :nombres
      t.string :cedula
      t.string :direccion
      t.string :telefono

      t.timestamps
    end
  end
end
