class ChangeFloatToDouble < ActiveRecord::Migration
  def up
  	 change_column :facturas, :subtotal, :decimal, :precision => 10, :scale => 2
  	 change_column :facturas, :iva, :decimal, :precision => 10, :scale => 2
  	 change_column :facturas, :total, :decimal, :precision => 10, :scale => 2
  	 change_column :productos, :precio, :decimal, :precision => 10, :scale => 2
  	 change_column :itemfacturas, :valortotal, :decimal, :precision => 10, :scale => 2
  	 change_column :itemfacturas, :precio_unitario, :decimal, :precision => 10, :scale => 2
  end

  def down
  	 change_column :facturas, :subtotal, :float
  	 change_column :facturas, :iva, :float
  	 change_column :facturas, :total, :float
  	 change_column :productos, :precio, :float
  	 change_column :itemfacturas, :valortotal, :float
  	 change_column :itemfacturas, :precio_unitario, :float
  end
end
