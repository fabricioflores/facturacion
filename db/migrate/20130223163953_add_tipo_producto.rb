class AddTipoProducto < ActiveRecord::Migration
  def up
  	change_table :productos do |t|
  		t.text "tipo"
  	end
  end

  def down
  	change_table :productos do |t|
  		t.remove "tipo"
  	end
  end
end
