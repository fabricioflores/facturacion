class CreateProductos < ActiveRecord::Migration
  def change
    create_table :productos do |t|
      t.text :descripcion
      t.float :precio
      t.integer :stock

      t.timestamps
    end
  end
end
