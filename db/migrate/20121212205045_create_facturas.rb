class CreateFacturas < ActiveRecord::Migration
  def change
    create_table :facturas do |t|
      t.integer :numero
      t.float :subtotal
      t.float :iva
      t.integer :cliente_id

      t.timestamps
    end
  end
end
