class AddReferencesProductoToProveedor < ActiveRecord::Migration
  def change
  	change_table :productos do |t|
        t.references :proveedor
    end
  end
end
