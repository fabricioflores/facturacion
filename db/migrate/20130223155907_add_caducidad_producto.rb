class AddCaducidadProducto < ActiveRecord::Migration
  def up
  	change_table :productos do |t|
  		t.datetime "fecha_caducidad"
  	end
  end

  def down
  	change_table :productos do |t|
  		t.remove "fecha_caducidad"
  	end
  end
end
